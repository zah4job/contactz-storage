from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet

from contact.models import Contact
from contact.serializers import ContactSerializer


class ContactViewSet(ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    permission_classes = [permissions.IsAuthenticated]
