from django.db import models


class Contact(models.Model):
    """Контактные данные соискателя."""

    class Meta:
        verbose_name = 'контакт'

    name = models.TextField('имя')
    phone = models.TextField('телефон')
    head_hunter_url = models.URLField('ссылка на резюме')
    cat = models.BooleanField('наличие кошки', default=False)
